# <a href="https://rspamd.com"><img src="https://rspamd.com/img/rspamd_logo_black.png" alt="Rspamd" width="220px"/></a>

## NAME

Rspamd web interface plugin for DirectAdmin Panel

## DESCRIPTION

This is a simple control interface for Rspamd spam filtering system. It provides basic functions for setting metric actions, scores, viewing statistic and learning.

## PLUGIN VERSION

- Version: 0.1.1
- Last modified: Tue May 14 12:30:43 +07 2019
- Update URL: https://files.poralix.com/get/freesoftware/rspamd.tar.gz
- Version URL: https://files.poralix.com/version/freesoftware/rspamd
- Tested with version of Rspamd: 1.9.2, 1.9.3 (stable) and 1.9.4 (experimental)

## INSTALLATION

Run the following commands as `root` to install the plugin from a `git` repository:

```
cd /usr/local/directadmin/plugins/
git clone https://github.com/poralix/rspamd.git
cd rspamd/scripts/
./install.sh
```

## USAGE

Install the plugin into DirectAdmin, and connect to it at an admin level.

## AUTHOR

Plugin for Directadmin is written by **Alex S Grebenschikov** - [poralix](https://github.com/poralix)

Rspamd with the web interface is written by **Vsevolod Stakhov** - [vstakhov](https://github.com/vstakhov)

## LICENSE

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

## CHANGELOG

- May 14, 2019: Updated for Rspamd 1.9.3 (stable) and 1.9.4 (experimental)

## REFERENCES

* Rspamd Home site: <https://rspamd.com>
* Plugin Development: <https://github.com/poralix/rspamd>
* Site repository: <https://github.com/rspamd/rspamd.com>

## OTHER

NOT FOR SELLING! FOR PRIVATE USAGE ONLY!
